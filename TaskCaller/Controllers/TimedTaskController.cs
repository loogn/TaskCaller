﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using TaskCaller.Models.entity;
using TaskCaller.Services;
using TaskCaller.UI;

namespace SchedulerLite.Manager.Controllers
{
    [Authorize]
    public class TimedTaskController : Controller
    {

        TimedTaskService _timedTaskService;
        public TimedTaskController(TimedTaskService timedTaskService)
        {
            _timedTaskService = timedTaskService;
        }


        // GET: TimedTask
        public ActionResult List(string name, bool? enable, int page = 1)
        {
            var pageSize = 10;
            var plist = _timedTaskService.SearchList(name, enable, page, pageSize);
            ViewBag.plist = plist.ToStaticPagedList();
            return View();
        }

        public ActionResult Edit(long id = 0)
        {
            var m = _timedTaskService.SingleById(id);
            if (m == null)
            {
                m = new TimedTask();
                m.Enable = true;
                m.Method = "GET";
                m.TimeoutSeconds = 15;
            }
            ViewBag.m = m;
            return View();
        }
        public ActionResult DoEdit(TimedTask m)
        {
            var ro = _timedTaskService.Edit(m);
            return Json(ro);
        }

        public ActionResult DoDelete(long id)
        {
            var ro = _timedTaskService.Delete(id);
            return Json(ro);
        }
    }
}