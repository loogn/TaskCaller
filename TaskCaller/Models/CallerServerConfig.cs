﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskCaller.Models
{
    public class CallerServerConfig
    {
        /// <summary>
        /// 加载定时任务的间隔秒数，0为不启用
        /// </summary>
        public int LoadTimedTaskIntervalSeconds { get; set; }
        public int LoadDelayTaskIntervalSeconds { get; set; }
        /// <summary>
        /// 延迟任务时间轮大小
        /// </summary>
        public int TimeWheelSlotNum { get; set; }
        /// <summary>
        /// 是否记录调用成功的日志
        /// </summary>
        public bool RecordSuccessLog { get; set; }
        
        /// <summary>
        /// sqlserver 或者 mysql
        /// </summary>
        public string DBType { get; set; }

    }
}
