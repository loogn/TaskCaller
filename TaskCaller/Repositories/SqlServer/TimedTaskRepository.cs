﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Loogn.OrmLite;
using Microsoft.Extensions.Options;
using TaskCaller.Models;
using TaskCaller.Models.entity;

namespace TaskCaller.Repositories.SqlServer
{
    public class TimedTaskRepository : BaseRepository, ITimedTaskRepository
    {
        public TimedTaskRepository(IOptions<ConnectionStringsConfig> options) : base(options)
        {
        }

        public long Add(TimedTask m)
        {
            using (var db = Open())
            {
                return db.Insert(m);
            }
        }

        public long Update(TimedTask m)
        {
            using (var db = Open())
            {
                return db.Update(m, "Name", "Cron", "Url", "Method", "PostData", "Enable", "UpdateTime", "SuccessFlag", "TimeoutSeconds");
            }
        }

        public OrmLitePageResult<TimedTask> SearchList(string name, bool? enable, int pageIndex, int pageSize)
        {
            var condition = "1=1";
            name = SqlInjection.Filter(name);
            if (!string.IsNullOrEmpty(name))
            {
                condition += " and Name like '%" + name + "%'";
            }
            if (enable != null)
            {
                condition += " and Enable=" + (enable.Value ? "1" : "0");
            }

            using (var db = Open())
            {
                return db.SelectPage<TimedTask>(new OrmLitePageFactor
                {
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    OrderBy = "id desc",
                    Conditions = condition,
                });
            }
        }

        public TimedTask SingleById(long id)
        {
            using (var db = Open())
            {
                return db.SingleById<TimedTask>(id);
            }
        }

        public List<TimedTask> SelectByIds(List<long> ids)
        {
            using (var db = Open())
            {
                return db.SelectByIds<TimedTask>(ids);
            }
        }

        public int Delete(long id)
        {
            using (var db = Open())
            {
                return db.DeleteById<TimedTask>(id);
            }
        }

        public Dictionary<string, List<long>> SelectReadyList()
        {
            using (var db = Open())
            {
                var sql = "SELECT Cron,Id FROM TimedTask WHERE Enable=1";
                return db.Lookup<string, long>(sql);
            }
        }

        public int ExecuteOne(TimedTask task)
        {
            using (var db = Open())
            {
                return db.UpdateById<TimedTask>(
                    DictBuilder.Assign("LastExecTime", task.LastExecTime)
                    .Assign("LastStatus", task.LastStatus)
                    .Assign("$ExecCount", "ExecCount+1"), task.Id);
            }
        }
    }
}
